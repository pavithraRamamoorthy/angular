import { getLocaleDateFormat } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {


  displayColumns = ['id', 'name', 'username', 'email', 'phone', 'website']
  displayData = ['id','name','username', 'email', 'phone', 'website'];
  constructor(private userService: UserService) { }
  users : User[] = [];
  

  ngOnInit(): void {
    this.userService.getUsers().subscribe
    (
      (response)=>
      {
        this.users = response;
      },

      (error)=>
      {
        console.log("Error Occured : "+error);
      }
    )
  }

  }

  // users = [
  //   { firstName: 'Pavithra', lastName: 'Rangineni', email: 'pavi@gmail.com', country: 'India' },
  //   { firstName: 'chenchu', lastName: 'Ramu', email: 'chenchuramu@gmail.com', country: 'India' },
  //   { firstName: 'chenchu', lastName: 'suresh', email: 'chenchu@gmail.com', country: 'India' },
  //   { firstName: 'Parshith', lastName: 'G.S', email: 'parshith@gmail.com', country: 'India' },
  //   { firstName: 'Pavithra', lastName: 'Ramamoorthy', email: 'pavithra@gmail.com', country: 'India' }
  // ];

