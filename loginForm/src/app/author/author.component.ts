import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  authorForm: FormGroup;
    
  constructor( private fb: FormBuilder ) {
    this.authorForm = this.fb.group({
      authorName: '',
      books: this.fb.array([]) ,
    });
  }

   books() : FormArray {
    return this.authorForm.get("books") as FormArray
  } 

  newBook(): FormGroup {
    return this.fb.group({
      name : ''
    })
  }

  addBook() {
    this.books().push(this.newBook());
  }

  ngOnInit(): void {
  }

  removeBook(i :number): void {
    (this.authorForm.get('books') as FormArray)?.removeAt(i);
  }

  handleSubmit() : void {
    console.log(this.authorForm.value)
  }
}

