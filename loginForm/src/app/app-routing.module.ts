import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { UserListComponent } from './user-list/user-list.component';
import { AuthorComponent } from './author/author.component';


const routes: Routes = [
  { 
    path: '', pathMatch: 'full', redirectTo: 'login' 
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path : 'signup',
    component : SignupComponent
  },
  {
    path : 'userlist',
    component : UserListComponent
  },
  {
    path : 'author',
    component : AuthorComponent
  },
  {
    path : '**',
    redirectTo : '/login',
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
