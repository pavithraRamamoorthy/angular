import { Component, OnInit } from '@angular/core';
import { FormControlName, FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.loginForm = new FormGroup(
      {
        email : new FormControl('',[Validators.required, Validators.email]),
        password : new FormControl('', [Validators.required, Validators.minLength(6)])
      }
    )
  }
  
  // formData = {
  //   emailAddress : '',
  //   password : '',

  // }

  onLogin(){
    console.log(this.loginForm.value);
    alert("Login SucessFully");
  }

}
