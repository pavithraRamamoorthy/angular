import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormControlName, FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;

  constructor(public fb: FormBuilder) { 
    this.signupForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNo: ['', [Validators.required, Validators.maxLength(10)]],
      password: ['', [Validators.required,Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    },
    { validators: passwordMatchValidator });  
  }

  
  ngOnInit(): void {
  }



  // formData = {
  //   firstName : '',
  //   lastName : '',
  //   email : '',
  //   password : '',
  //   confirm_password : '',
  //   contact_no : ''
  // }

  click(){
    alert("Registration sucessfully")
  }

  onSubmit(){
    console.log(this.signupForm.value);
  }
}

export const passwordMatchValidator: Validators = (signupForm: FormGroup) => {
  return signupForm.get('password').value === signupForm.get('confirmPassword').value ?
    null : { 'passwordMismatch': true };
}
